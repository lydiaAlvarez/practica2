package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    // enlace a POSTMAN para que nos devuelva la función
    static final String APIBaseURL = "/apitechu/v1";

    //CRUD = Create, Read, Update, Delete (Mantenimiento) (GET, POST, PATH,etc)

    @GetMapping(APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

    //    return new ArrayList<>();
        return ApitechuApplication.productModels;
    }

    // para buscar por ID
    @GetMapping(APIBaseURL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");

        // con el '+' concatenamos el valor de la variable, al igual que en JS con el '.'
        System.out.println("La ID del producto es: " + id);

        ProductModel result = new ProductModel();

        // creamos un 'foreach' definiendo 'products' como cada elemento de productModel y así poder
        // realizar la búsqueda en el array
        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                System.out.println("Producto encontrado");
                result = product;
            }
        }
        //devolvemos el valor que ya tenemos definido en ProductModel
        return result;

        // Devolvemos un nuevo producto por defecto
        //return new ProductModel();

    }

    // para crear un nuevo elemento
    @PostMapping(APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La ID que definimos en el producto recién creado es: " + newProduct.getId());
        System.out.println("La Desc que definimos en el producto recién creado es: " + newProduct.getDesc());
        System.out.println("El valor que definimos en el producto recién creado es: " + newProduct.getPrice());


        // Devolvemos un valor por defecto
        //return new ProductModel();

        // añadimos el nuevo producto
        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProducts(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("Update Products");
        System.out.println("La ID del producto a actualizar es: " + id);
        System.out.println("La ID del producto actualizado es: " + product.getId());
        System.out.println("La desc del producto actualizada es: " + product.getDesc());
        System.out.println("El precio del producto actualizado es: " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        // devolvemos un valor por defecto
        //return new ProductModel();

        //ApitechuApplication.productModels.replaceAll(product);
        return product;

    }

    // tenemos que hacer una actualizacion parcial, cambiar el precio y/o la descripcion
    // 1- no actualizamos nada
    // 2- actualizamos solo el precio
    // 3- actualizamos solo la descripcion
    // 4- actualizamos precio y descripcion

    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("Update Parcial");
        System.out.println("La ID del producto es: " + id);
        System.out.println("La descripcion actualizada es: " + product.getDesc());
        System.out.println("El precio actualizado es: " + product.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = productInList;

                if (productInList.getDesc() != null) {
                    System.out.println("Actualizamos la descripción");
                    productInList.setDesc(product.getDesc());
                }

                if (productInList.getPrice() > 0) {
                    System.out.println("Actualizamos el precio");
                    productInList.setPrice(product.getPrice());
                }
            }
        }
        return result;
    }

    @DeleteMapping(APIBaseURL + "/products/{id}")
    public ProductModel deleteProducts(@PathVariable String id){
        System.out.println("Delete de productos");
        System.out.println("La ID del producto a borrar es: " + id);

        ProductModel result = new ProductModel();
        boolean foundNumber = false;

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)){
                System.out.println("Hemos encontrado el producto");
                foundNumber = true;
            }
        }

        if (foundNumber){
            System.out.println("Producto borrado con éxito");
        }

        return new ProductModel();
    }
}
