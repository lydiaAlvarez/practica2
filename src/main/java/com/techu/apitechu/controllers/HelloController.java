package com.techu.apitechu.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hello World from API TECH U";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "TechU") String name){
        return String.format("Hello %s", name);
    }
}
